#!/bin/bash
# A simple shell script for installing & configuring
# the project at https://gitlab.com/bizzare-pizza/bizzare-api

echo "NOTE: The script is going to import project here!"

read -p "Does current working directory suit you? (y/n) " ANSWER

if [[ $ANSWER != "y" ]] && [[ $ANSWER != "Y" ]]
then
    exit 1
fi


echo -e "\e[5mPreparing your virtual environment...\e[0m"

python3 -m venv bizzare-api-env
source bizzare-api-env/bin/activate

EXPECTED_ENV=$(pwd)"/bizzare-api-env"

if [[ $VIRTUAL_ENV == $EXPECTED_ENV ]]
then
    echo -ne "\e[1A\e[K\r\e[1;92mEnvironment prepared successfully!\e[0m\n"
else
    echo -ne "\e[1;31mThere was an error while entering the virtual environment.\e[0m\n"
    deactivate
    [ -d bizzare-api-env ] && rm -Rf bizzare-api-env
    exit 1
fi

cd bizzare-api-env
git clone https://gitlab.com/bizzare-pizza/bizzare-api.git

cd bizzare-api
pip3 install -r resources/misc/requirements.txt

mkdir resources/private

echo ""
echo -e "\e[1;92mScript finished successfully!\e[0m"
echo -e "\e[1mNOTE: To remove the project, simply remove the bizzare-api-env directory.\e[0m"
