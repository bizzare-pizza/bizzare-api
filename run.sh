#!/bin/bash

source ../bin/activate
export FLASK_ENV=development

if [[ $1 == "--load-db" ]]
then
    echo -e "\e[1;37mLaunching «Bizzare Pizza» API server…"
    echo -e "Press Ctrl+C to shut it down!\e[0;37m"
    python3 main.py --load-db
elif [[ $1 == "--nodrop" ]]
then
    echo -e "\e[1;37mLaunching «Bizzare Pizza» API server without dropping the database…"
    echo -e "Press Ctrl+C to shut it down!\e[0;37m"
    python3 main.py --nodrop
elif [[ $1 == "" ]]
then
    echo -e "\e[1;37mLaunching «Bizzare Pizza» API server with empty database…"
    echo -e "Press Ctrl+C to shut it down!\e[0;37m"
    python3 main.py
else
    echo "Unknown arguments provided!"
    echo ""
    echo "  --load-db  —  Reload the database w/ test data"
    echo "  --nodrop   —  Don't drop the existing data"
    echo ""
fi
